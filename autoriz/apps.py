from django.apps import AppConfig


class AutorizConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'autoriz'
