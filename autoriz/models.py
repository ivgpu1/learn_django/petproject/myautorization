from unicodedata import name
from django.db import models

class Person(models.Model):
    name = models.CharField(max_length = 30)
    number = models.CharField(max_length = 11)
    password = models.CharField(max_length = 50)
