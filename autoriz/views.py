import re
from django.http import HttpResponse
from django.shortcuts import render
from .models import Person
from .dto import Person_Dto

def index(request):
    return render(request, "index.html")

def login(request):
    return render(request, "login.html")

def post_login(request):
    number = request.POST.get("number", "79000000000")
    password = request.POST.get("password", "0000")
    test = Person_Dto(number, password)
    return HttpResponse(check(test))

def registration(request):
    return render(request, "registration.html")

def post_register(request):
    r_name = request.POST.get("name", "Undefined")
    r_number = request.POST.get("number", "79000000000")
    r_password = request.POST.get("password", "0000")
    test = Person.objects.create(name = r_name, number = r_number, password = r_password)
    return HttpResponse(f"<h2>{test.name}, успешно зарегистрировались :) </h2>")

def check(person: Person_Dto):
    if Person.objects.get(number = person.number): 
        if Person.objects.get(password = person.password):
            return "<h2>Вы вошли в свой аккаунт :) </h2>"
    else:
        return "<h2>Вам необходимо зарегистрироваться )"
